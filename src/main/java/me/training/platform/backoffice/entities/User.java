package me.training.platform.backoffice.entities;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import me.training.platform.backoffice.validator.FieldsValueMatch;
import me.training.platform.backoffice.validator.Matches;

@Entity
@Table(name="users")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")

@FieldsValueMatch.List({
        @FieldsValueMatch(
                field = "password",
                fieldMatch = "confirm_password",
                message = "Passwords do not match!"
        )
})
//@Matches(fields={"password", "confirm_password"}, verifyFields={"password", "confirm_password"})
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    @ApiModelProperty(notes="Name should have atleast 2 characters, max 5 characters")
    @Size(min=2, max = 5, message="Name should have atleast 2 characters, max 5 characters")
    private String name;

    @Column(name = "surname")
    @ApiModelProperty(notes="Surname should have atleast 2 characters, max 5 characters")
    //@Size(min=2, message="Surname should have atleast 2 characters")
    @Size(min=2, max = 5, message="Name should have atleast 2 characters, max 5 characters")
    private String surname;

    @Column(name = "email")
    @ApiModelProperty(notes="Email should have valid email format")
    @NotEmpty(message = "Email is a required field")
    @Email(message = "Email is a email pattern")
    private String email;

    @Column(name = "password")
    @NotEmpty(message = "Password is a required field")
    private String password;

    @Transient
    private String confirm_password;

    /*@AssertTrue(message="confirm_password field should be equal than password field")
    private boolean isValid() {
        return this.password.equals(this.confirm_password);
    }*/

    @Column(name = "age")
    //@NumberFormat(style = NumberFormat.Style.NUMBER)
    //@Pattern(regexp = "[+-]?[0-9][0-9]*", message = "Age is a numeric")
    @Digits(integer=2, fraction=0, message = "Age is a only numeric and between 15 to 60 years old")
    //@NotEmpty(message = "Age is a required field, Number format")
    //@Range(min=15, max=30, message = "Age is a numeric and between 15 to 60 years old")
    private Integer age;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    @NotEmpty(message = "City is a required field")
    private String city;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "active")
    private Integer active;

    @Column(name = "api_token")
    private String api_token;

    //@JsonProperty("remember_token")
    @Column(name = "remember_token")
    private String rememberToken;

    //@JsonProperty("created_at")
    @Column(name = "created_at")
    private Timestamp createdAt;

    //@JsonProperty("updated_at")
    @Column(name = "updated_at")
    private Timestamp updatedAt;
}