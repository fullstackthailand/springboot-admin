package me.training.platform.backoffice.controllers.admin;

import me.training.platform.backoffice.entities.Pager;
import me.training.platform.backoffice.entities.User;
import me.training.platform.backoffice.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@Controller
@RequestMapping("/admin/user")
public class UserController {
    @Autowired
    private UsersRepository usersRepository;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    /*@GetMapping(value = "")
    public String index(Model model) throws Exception {
        List<User> users = usersRepository.findAll();
        //System.out.println(users);
        model.addAttribute("items", users);

        return "admin/user/lists";
    }*/

    @GetMapping(value = "/hello")
    public String hello(Model model) throws Exception {

        List<User> users = usersRepository.findAll();
        model.addAttribute("items", users);

        return "admin/user/lists-test";
    }

    @GetMapping(value = "")
    public String index(
            Model model,
            @RequestParam("pageSize") Optional<Integer> pageSize,
            @RequestParam("page") Optional<Integer> page) throws Exception {

        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;


        Page<User> users = usersRepository.findAll(PageRequest.of(evalPage, evalPageSize));
        //List<User> users = usersRepository.findAll();
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        System.out.println("XXXXXXXXX");
        System.out.println(users);
        System.out.println("YYYYYYYYY");
        model.addAttribute("items", users);
        model.addAttribute("selectedPageSize", evalPageSize);
        model.addAttribute("pageSizes", PAGE_SIZES);
        model.addAttribute("pager", pager);

        return "admin/user/lists";
    }

    @GetMapping(value = "/create")
    public String create(Model model, User user) throws Exception {
        model.addAttribute("cities", getCities());
        model.addAttribute("user", user);

        return "admin/user/create";
    }

    @PostMapping(value = "")
    public String store(@Valid User user, BindingResult bindingResult, Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            model.addAttribute("cities", getCities());
            model.addAttribute("user", user);

            return "admin/user/create";
        }

        return "Save Success.";
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable String id, Model model) throws Exception {
        Optional<User> user = usersRepository.findById(Integer.parseInt(id));
        System.out.println(user);
        model.addAttribute("item", user);
        System.out.println(model);

        return "admin/user/edit";
    }

    @PutMapping(value = "/{id}")
    public String update() throws Exception {
        return "PUT Update";
    }

    private Map<String,String> getCities() {
        Map<String,String> cities = new HashMap<String, String>();
        cities.put("bangkok", "Bangkok");
        cities.put("nakornpathom", "Bakornpathom");

        return cities;
    }
}
