package me.training.platform.backoffice.controllers.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import me.training.platform.backoffice.entities.User;
import me.training.platform.backoffice.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@RestController("api-user")
@RequestMapping("/api/user")
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Persons.")
public class UserController {
    @Autowired
    private UsersRepository usersRepository;
    private static final Logger logger = LogManager.getLogger(UserController.class);


    @GetMapping(value = "")
    @ApiOperation("Returns list of all Users in the system.")
    public List<User> index(@RequestParam Map<String,String> inputs) throws Exception {
        logger.info("Info log: List all users");
        return usersRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    @ApiOperation("Returns a specific user by their identifier. 404 if does not exist.")
    public Optional<User> getById(@ApiParam("Id of the person to be obtained. Cannot be empty.")
                                    @PathVariable String id, @RequestParam Map<String,String> inputs) throws Exception {

        logger.info("Info log: List user by Id");
        return usersRepository.findById(Integer.parseInt(id));
    }

    @PutMapping(value = "/{id}")
    public String update(@PathVariable String id) throws Exception {

        return "PUT Update";
    }

    @DeleteMapping(value = "/{id}")
    public String destroy(@PathVariable String id) throws Exception {

        return "DELETE destroy";
    }
}
