package me.training.platform.backoffice.repositories;

import me.training.platform.backoffice.entities.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopsRepository extends JpaRepository<Shop,Integer> {

}
