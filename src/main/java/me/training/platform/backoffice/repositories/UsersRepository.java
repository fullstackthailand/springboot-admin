package me.training.platform.backoffice.repositories;

import me.training.platform.backoffice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User,Integer> {

    User findByEmail(String email);
    //User save(User users);
}
